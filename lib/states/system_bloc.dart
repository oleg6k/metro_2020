import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';
import 'package:metro_2020/data/providers/database/gateways/system.dart';
import 'package:rxdart/rxdart.dart';

class SystemData {
  bool firstStart;
  bool darkTheme;
  String locale;
  String phone;

  SystemData(
    this.firstStart,
    this.darkTheme,
    this.locale,
    this.phone,
  );
}

class SystemBloc {
  final SystemGateway systemGateway;

  SystemData _systemData;
  BehaviorSubject<SystemData> _systemSubject;

  SystemBloc({
    @required this.systemGateway,
  }) {
    _systemSubject = new BehaviorSubject<SystemData>();
    update();
  }

  Future<void> update({bool addCustom = false}) async {
    final data = await Future.wait([
      systemGateway.getFirstStartState(),
      systemGateway.getDarkThemeState(),
      systemGateway.getLocale(),
      systemGateway.getPhone(),
    ]);

    print(data);

    _systemData = SystemData(data[0], data[1], data[2], data[3]);
    _systemSubject.sink.add(_systemData);
  }

  void setDarkTheme(bool state) async {
    await systemGateway.setDarkThemeState(state);
    _systemData.darkTheme = state;
    _systemSubject.sink.add(_systemData);
  }

  Future<void> setLocale(String locale) async {
    await systemGateway.setLocaleLanguage(locale);
    _systemData.locale = locale;
    _systemSubject.sink.add(_systemData);
  }

  Future<void> setPhone(String phone) async {
    await systemGateway.setPhone(phone);
    _systemData.phone = phone;
    _systemSubject.sink.add(_systemData);
  }

  ValueStream<SystemData> get data => _systemSubject.stream;

  void close() {
    _systemSubject.close();
  }
}
