import 'package:flutter/material.dart';
import 'package:metro_2020/common/constants.dart';
import 'package:metro_2020/common/injector.dart';
import 'package:metro_2020/widgets/auth/welcome.dart';
import 'package:metro_2020/widgets/metro_map/metro.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await setupInjector();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void dispose() {
    closeGlobalBlocs();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Метро 2020',
      theme: ThemeData(
        primarySwatch: white,
        primaryIconTheme: const IconThemeData(
          color: Colors.redAccent
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Welcome(),
    );
  }
}