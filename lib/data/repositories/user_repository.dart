import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:meta/meta.dart';
import 'package:metro_2020/data/models/User.dart';
import 'package:metro_2020/data/providers/storage/storage_data_provider.dart';
import 'package:metro_2020/data/providers/http/http_data_provider.dart';

loadList(String storageEntities, dynamic model) {
  if (storageEntities != null) {
    return (json.decode(storageEntities) as List).map((storageEntity) => model.fromJson(Map<String, dynamic>.from(storageEntity as Map))).toList();
  } else {
    return [];
  }
}

class UserRepository {
  final storageDataProvider = StorageDataProvider();
  User localUser = new User();

  User get user => this.localUser;
  String localUserID;

  Future<void> loadUser() async {
    var user = await storageDataProvider.read('user');
    if (user != null) {
      this.localUser = User.fromJson(json.decode(user));
    }
  }

  Future<void> saveUser(String userData) async {
    await saveToProtectedStorage('user', userData);
    await loadUser();
  }

  Future<void> deleteFromProtectedStorage(String key) async {
    await storageDataProvider.delete(key);
    return;
  }

  Future<void> deleteTokens() async {
    await deleteFromProtectedStorage('access');
    await deleteFromProtectedStorage('refresh');
    return;
  }

  Future<void> deleteUser() async {
    await deleteFromProtectedStorage('user');
    return;
  }

  Future<void> saveToProtectedStorage(String key, String value) async {
    await storageDataProvider.write(key, value);
    return;
  }

  Future<String> getAccessToken() async {
    return await storageDataProvider.read('access');
  }

  Future<bool> hasAccessToken() async {
    var accessToken = await storageDataProvider.read('access');
    return null != accessToken;
  }

  Future<void> updateAccess() async {
    var refreshToken = await storageDataProvider.read('refresh');
    print(refreshToken);
    var refreshResponse = await HttpDataProvider.post('refreshTokens', {'refresh': refreshToken.toString()});
    await saveToProtectedStorage('access', refreshResponse['access']);
    await saveToProtectedStorage('refresh', refreshResponse['refresh']);
    const tenMinutes = const Duration(minutes: 10);
    new Timer.periodic(tenMinutes, (Timer t) => updateAccess());
    return;
  }

  Future<Map<String, dynamic>> registration({
    @required String email,
    @required String password,
    @required String phone,
  }) async {
    var body = phone != null ? {'email': email, 'password': password, 'phone': phone, 'mobile_token': localUserID} : {'email': email, 'password': password, 'mobile_token': localUserID};
    var registrationResponse = await HttpDataProvider.post('reg', body);
    await saveUser(json.encode(registrationResponse['user']));
    return {'access': registrationResponse['access'], 'refresh': registrationResponse['refresh']};
  }

  Future<Map<String, dynamic>> authenticate({
    @required String login,
    @required String password,
  }) async {
    var loginResponse = await HttpDataProvider.post('login', {'login': login, 'password': password, 'mobile_token': localUserID});
    await saveUser(json.encode(loginResponse['user']));
    return {'access': loginResponse['access'], 'refresh': loginResponse['refresh'], 'userString': json.encode(loginResponse['user'])};
  }

  String generateHash() {
    var random = Random.secure();
    var values = List<int>.generate(12, (i) => random.nextInt(255));
    return base64UrlEncode(values);
  }

  Future<Map<String, dynamic>> autoLogin(String mobileToken) async {
    var loginResponse = await HttpDataProvider.post('login', {'mobile_token': mobileToken});
    await saveUser(json.encode(loginResponse['user']));
    if (loginResponse['user']['mobile_token'] != null) {
      await saveToProtectedStorage('localUserID', loginResponse['user']['mobile_token']);
    }
    return {'access': loginResponse['access'], 'refresh': loginResponse['refresh'], 'userString': json.encode(loginResponse['user'])};
  }
}
