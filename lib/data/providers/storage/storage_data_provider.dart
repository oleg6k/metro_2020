import 'package:flutter_secure_storage/flutter_secure_storage.dart';
class StorageDataProvider {
  // Create storage
  final storage = new FlutterSecureStorage();

  // Read value
  Future<String> read(String key) async {
    return await storage.read(key: key);
  }

  // Read all values
  Future<Map<String, String>> readAll() async {
    return await storage.readAll();
  }

  // Delete value
  Future<void> delete(String key) async {
    await storage.delete(key: key);
    return;
  }

  // Delete all
  Future<void> deleteAll() async {
    await storage.deleteAll();
    return;
  }

  // Write value
  Future<void> write(String key, String value) async {
    await storage.write(key: key, value: value);
    return;
  }
}
