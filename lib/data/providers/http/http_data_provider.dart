import 'dart:ui' as ui;

import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

import 'package:metro_2020/data/repositories/user_repository.dart';

class HttpDataProvider {
  static Map<String, String> headers = {};
  static String baseUrl = DotEnv().env['BASE_URL'];
  static UserRepository userRepository = UserRepository();

  static Map<String, String> urls = {
    "signUp": "/users/signUp",
    "signIn": "/users/signIn",
    "user": "/users/user",
    "whoAmI": "/users/whoAmI",
    "users": "/users",
  };

  static get(String urlKey) async {
    print('GET REQUEST '+urlKey);
    if(await userRepository.hasAccessToken()){
      headers = {'Authorization': 'Bearer '+ await userRepository.getAccessToken()};
    }
    var response = await http.get(
      baseUrl + urls[urlKey],
      headers: headers,
    );
    return parseResponse(response);
  }

  static post(String urlKey, Object body) async {
    print('POST REQUEST '+urlKey);
    if(await userRepository.hasAccessToken()){
      headers = {'Authorization': 'Bearer '+ await userRepository.getAccessToken()};
    }
    print('body $body ');
    var response = await http.post(baseUrl + urls[urlKey], headers: headers, body: body);
    return parseResponse(response);
  }

  static parseResponse(http.Response response) {
    var body = jsonDecode(response.body);
    body = new Map<String, dynamic>.from(body);
    if (response.statusCode == 400) {
      print(body);
      throw new http.ClientException(body['data']);
    } else if (response.statusCode == 401) {
      throw new http.ClientException('Not authorized!');
    } else if (response.statusCode != 200) {
      print(body);
      throw new http.ClientException('Unknown error.');
    }
    return body['data'];
  }
}
