import 'dart:ui' as ui;

import 'package:meta/meta.dart';
import 'package:metro_2020/common/constants.dart';
import 'package:metro_2020/data/providers/database/db_adapter.dart';

class SystemGateway {
  final DbAdapter db;

  SystemGateway({@required this.db});

  Future<String> getLocale() async {
    final db = await this.db.database;

    return db.transaction((tx) async {
      final sysLng = ui.window.locale.languageCode;
      final rs = await tx.rawQuery('SELECT value FROM system WHERE key = ?', [System.LOCALE]);
      if (rs.length == 0) {
        await tx.rawInsert(
          'INSERT INTO system (key, value) VALUES (?, ?)',
          [System.LOCALE, sysLng]
        );
        return sysLng;
      }
      return  rs.length > 0 ? rs.first['value'] : sysLng;
    });
  }

  Future<bool> getFirstStartState([bool defaultState = true]) async {
    final db = await this.db.database;
    return db.transaction((tx) async {
      final rs = await tx.rawQuery('SELECT value FROM system WHERE key = ?', [System.FIRST_START]);
      if (rs.length == 0) {
        await tx.rawInsert(
          'INSERT INTO system (key, value) VALUES (?, ?)',
          [System.FIRST_START, defaultState.toString()]
        );
        return defaultState;
      }
      return rs.first['value'] == 'true';
    });
  }

  Future<void> setFirstStartState(bool state) async {
    final db = await this.db.database;
    await db.rawUpdate('UPDATE system SET value = ? WHERE key = ?', [state.toString(), System.FIRST_START]);
  }

  Future<void> setPhone(String phone) async {
    final db = await this.db.database;
    await db.rawUpdate('UPDATE system SET value = ? WHERE key = ?', [phone, System.PHONE]);
  }

  Future<String> getPhone() async {
    final db = await this.db.database;
    final rs = await db.rawQuery('SELECT value FROM system WHERE key = ?', [System.PHONE]);
    return  rs.length > 0 ? rs.first['value'] : null;
  }

  Future<bool> getDarkThemeState([bool defaultState = true]) async {
    final db = await this.db.database;
    return db.transaction((tx) async {
      final rs = await tx.rawQuery('SELECT value FROM system WHERE key = ?', [System.DARK_THEME]);
      if (rs.length == 0) {
        await tx.rawInsert(
          'INSERT INTO system (key, value) VALUES (?, ?)',
          [System.DARK_THEME, defaultState.toString()]
        );
        return defaultState;
      }
      return rs.first['value'] == 'true';
    });
  }

  Future<void> setDarkThemeState(bool state) async {
    final db = await this.db.database;
    await db.rawUpdate('UPDATE system SET value = ? WHERE key = ?', [state.toString(), System.DARK_THEME]);
  }

  Future<void> setLocaleLanguage(String locale) async {
    final db = await this.db.database;
    await db.rawUpdate('UPDATE system SET value = ? WHERE key = ?', [locale.toString(), System.LOCALE]);
  }
}
