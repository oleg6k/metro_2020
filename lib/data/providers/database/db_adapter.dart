import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:metro_2020/common/constants.dart';

// '''
//   CREATE TABLE system (
//     key TEXT NOT NULL,
//     value TEXT NOT NULL,
//     PRIMARY KEY (key)
//   )
// '''

/// тут логика создания БД
final initialisation = ['''
  CREATE TABLE system (
    key TEXT NOT NULL,
    value TEXT NOT NULL,
    PRIMARY KEY (key)
  )
'''
];

/// тут миграции
final migrations = [];

class DbAdapter {
  Future<Database> database;

  DbAdapter() {
    database = open();
  }

  Future<Database> open() async {
    final db = await openDatabase(
      join(await getDatabasesPath(), SQLITE_DB_NAME),
      version: migrations.length + 1,
      onUpgrade: (Database db, int oldVersion, int newVersion) async {
        if (oldVersion == 0) {
          initialisation.forEach((sql) async => await db.execute(sql));
          migrations.forEach((sql) async => await db.execute(sql));
        } else {
          for (var i = oldVersion - 1; i < newVersion - 1; i++) {
            await db.execute(migrations[i]);
          }
        }
      }
    );
    await db.rawQuery('PRAGMA foreign_keys = ON');
    return db;
  }
}
