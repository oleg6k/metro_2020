class User{
  final int id;
  final String email;
  final String phone;
  final String surname;
  final String name;

  const User({this.id, this.email, this.phone, this.surname, this.name});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'] as int,
      email: json['email'] as String,
      phone: json['phone'] as String,
      surname: json['surname'] as String,
      name: json['name'] as String
    );
  }
}
