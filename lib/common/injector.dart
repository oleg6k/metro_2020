import 'package:injector/injector.dart';
import 'package:metro_2020/data/providers/database/db_adapter.dart';
import 'package:metro_2020/data/providers/database/gateways/system.dart';
import 'package:metro_2020/states/system_bloc.dart';


Future<void> setupInjector() async {
  Injector injector = Injector.appInstance;

  injector.registerSingleton<DbAdapter>((_) => DbAdapter());

  injector.registerSingleton<SystemGateway>((Injector injector) {
    return SystemGateway(db: injector.getDependency<DbAdapter>());
  });

  injector.registerSingleton<SystemBloc>((Injector injector) {
  return SystemBloc(
      systemGateway: injector.getDependency<SystemGateway>()
    );
  });
}

void closeGlobalBlocs() {
  Injector injector = Injector.appInstance;
  injector.getDependency()<SystemBloc>().close();
}
