import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RoundedElevationButtonBase extends StatelessWidget {

  const RoundedElevationButtonBase({
    Key key,
    this.pathToIcon = 'assets/icons/Vector-1.svg',
    this.onPress,
    this.color,
    this.colorIcon,
  }) : super(key: key);

  final String pathToIcon;
  final void Function() onPress;
  final Color color;
  final Color colorIcon;

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Container(
        width: 48,
        height: 48,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(24),
          color: Colors.transparent,
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black,
              blurRadius: 15,
              offset: Offset(2, 4)
            )
          ],
        ),
        child: Material(
          color: color ?? Colors.white,
          child: InkWell(
            onTap: onPress,
            child: Container(
              padding: const EdgeInsets.all(14),
              child: SvgPicture.asset(
                pathToIcon,
                color: colorIcon ?? Colors.black,
              )
            ),
          ),
        ),
      ),
    );
  }
}
