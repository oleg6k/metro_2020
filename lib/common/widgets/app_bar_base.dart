import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppBarBase extends StatelessWidget implements PreferredSizeWidget {
  final double height;

  const AppBarBase({
    Key key,
    @required this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.grey[300],
          child: AppBar(
            title: Text('Схема метро'),
            actions: [
              // IconButton(
              //   icon: Icon(Icons.verified_user),
              //   onPressed: () => null,
              // ),
            ],
          ) ,
        ),
      ],
    );
  }
  
 @override
  Size get preferredSize => Size.fromHeight(height);
}
