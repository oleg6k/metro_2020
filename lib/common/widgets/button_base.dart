import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonBase extends StatelessWidget {
  const ButtonBase({
    Key key,
    this.onPressed,
    this.fillColor = Colors.white,
    this.textColor = Colors.redAccent,
    this.splashColor = Colors.red,
    this.borderColor = Colors.red,
    @required this.text,
  }) : super(key: key);

  final void Function() onPressed;
  final Color fillColor;
  final Color textColor;
  final Color splashColor;
  final Color borderColor;
  final String text;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
        side: BorderSide(
          color: borderColor
        )
      ),
//     height: 44,
//     minWidth: MediaQuery.of(context).size.width,
      color: fillColor,
      textColor: textColor,
      disabledColor: Colors.grey,
      disabledTextColor: Colors.black,
      padding: EdgeInsets.all(8.0),
      splashColor: splashColor,
      onPressed: onPressed,
      child: Text(
        text,
        style: TextStyle(
          fontSize: 16.0,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}