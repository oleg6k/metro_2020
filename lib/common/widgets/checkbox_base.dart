import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckBoxBase extends StatelessWidget {
  const CheckBoxBase({
    Key key,
    @required this.value,
    @required this.onChanged,
    this.bgColor = Colors.white,
    this.bColor,
    this.cColor,
  }) : super(key: key);

  final bool value;
  final void Function() onChanged;
  final Color bgColor;
  final Color bColor;
  final Color cColor;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onChanged,
      child: Container(
        width: 22,
        height: 22,
        decoration: BoxDecoration(
          color: bgColor,
          border: Border.all(
            width: 2,
            color: bColor ?? Colors.grey[400]
          ),
          borderRadius: BorderRadius.circular(5)
        ),
        child: AnimatedOpacity(
          duration: Duration(milliseconds: 300),
          opacity: value ? 1 : 0,
          child: Container(
            child: Icon(
              Icons.check,
              color: cColor ?? Colors.grey[500],
              size: 20,
            ),
          ),
        ),
      ),
    );
  }
}