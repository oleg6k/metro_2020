import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro_2020/common/widgets/app_bar_base.dart';
import 'package:metro_2020/widgets/FromTo.dart';
import 'package:metro_2020/widgets/settings/settings.dart';

class ScaffoldBase extends StatelessWidget {
  const ScaffoldBase({
    Key key,
    this.body,
    this.useDrawer = true,
    this.keyScaffold,
    this.backgroundColor,
    this.appBar,
  }) : super(key: key);

  final Widget body;
  final bool useDrawer;
  final GlobalKey<ScaffoldState> keyScaffold;
  final Color backgroundColor;
  final PreferredSizeWidget appBar;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: backgroundColor,
        key: keyScaffold,
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                padding: EdgeInsets.all(0.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    image: DecorationImage(
                      image: AssetImage("assets/profile_bg/bg.png"),
                        fit: BoxFit.cover
                      )
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20, bottom: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        ClipOval(
                          child: Container(
                            width: 64,
                            height: 64,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(32),
                              color: Colors.grey[300],
                              border: Border.all(
                                width: 2,
                                color: Colors.white
                              ),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Image.asset('assets/no_avatar.png'),
                            ),
                          ),
                        ),
                        SizedBox(height: 16),
                        Text(
                          'Евгений',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 18
                          ),
                        ),
                        Text(
                          '+7 (920) 624 15 04',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 14
                          ),
                        ),
                      ]
                    ),
                  ),
                ),
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                ),
              ),
              ListTile(
                title: Row(
                  children: [
                    Container(
                      width: 30,
                      height: 30,
                      child: SvgPicture.asset('assets/icons/M.svg')
                    ),
                    SizedBox(width: 14),
                    Text('Схема метро')
                  ]
                ),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
              ListTile(
                title: Row(
                  children: [
                    Container(
                      width: 22,
                      height: 22,
                      child: SvgPicture.asset('assets/icons/Vector.svg')
                    ),
                    SizedBox(width: 22),
                    Text('Мои предпочтения')
                  ]
                ),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title: Row(
                  children: [
                    Container(
                      width: 22,
                      height: 22,
                      child: SvgPicture.asset('assets/icons/Vector-1.svg')
                    ),
                    SizedBox(width: 22),
                    Text('Мои поездки')
                  ]
                ),
                onTap: () {
                }
              ),
              ListTile(
                title: Row(
                  children: [
                    Container(
                      width: 22,
                      height: 22,
                      child: SvgPicture.asset('assets/icons/Vector-2.svg')
                    ),
                    SizedBox(width: 22),
                    Text('Мои сообщения')
                  ]
                ),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title: Row(
                  children: [
                    Container(
                      width: 22,
                      height: 22,
                      child: SvgPicture.asset('assets/icons/Vector-3.svg')
                    ),
                    SizedBox(width: 22),
                    Text('Мои адреса')
                  ]
                ),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title:  Row(
                  children: [
                    Container(
                      width: 22,
                      height: 22,
                      child: SvgPicture.asset('assets/icons/credit_card.svg')
                    ),
                    SizedBox(width: 22),
                    Text('Мой проездной')
                  ]
                ),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title:  Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      width: 22,
                      height: 22,
                      child: SvgPicture.asset('assets/icons/user.svg')
                    ),
                    SizedBox(width: 22),
                    Text('Профиль')
                  ]
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Settings())
                  );
                },
              ),
            ], 
          ),
        ),
        appBar: useDrawer ? AppBarBase(height: 56) : this.appBar != null ? appBar : AppBarBase(height: 0),
        body: body,
      ),
    );
  }
}
