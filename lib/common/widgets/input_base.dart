import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:metro_2020/common/station.dart';

class InputBase extends StatefulWidget {
  InputBase({
    Key key,
    this.valueText,
    this.mask,
    this.validator,
    this.label,
    this.inputType,
    this.onFieldSubmitted,
    this.maxLenght,
    this.focusNode,
    this.textInputAction,
    this.textAlign,
    this.hint,
    this.useClearIcon = false,
    this.controller,
    this.usePrefixSearch = false,
  }) : super(key: key);

  final String valueText;
  final MaskTextInputFormatter mask;
  final String Function(String) validator;
  final String label;
  final TextInputType inputType;
  final void Function(String) onFieldSubmitted;
  final int maxLenght;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final TextAlign textAlign;
  final String hint;
  final bool useClearIcon;
  final TextEditingController controller;
  final bool usePrefixSearch;

  @override
  _InputBaseState createState() => _InputBaseState();
}

class _InputBaseState extends State<InputBase> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return widget.label != null ?  Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 8.0),
          child: Text(
            widget.label,
            style: TextStyle(
              color: Colors.grey[500],
              fontWeight: FontWeight.w400,
              fontSize: 14
            ),
          ),
        ),
        Input(
          valueText: widget.valueText,
          mask: widget.mask,
          validator: widget.validator,
          label: widget.label,
          inputType: widget.inputType,
          onFieldSubmitted: widget.onFieldSubmitted,
          maxLenght: widget.maxLenght,
          formKey: formKey,
          textInputAction: widget.textInputAction,
          textAlign: widget.textAlign,
          hint: widget.hint,
          focusNode: widget.focusNode,
          useClearIcon: widget.useClearIcon,
          controller: widget.controller,
          usePrefixSearch: widget.usePrefixSearch
        ),
      ]
    ) : Input(
        valueText: widget.valueText,
        mask: widget.mask,
        validator: widget.validator,
        label: widget.label,
        inputType: widget.inputType,
        onFieldSubmitted: widget.onFieldSubmitted,
        maxLenght: widget.maxLenght,
        formKey: formKey,
        textInputAction: widget.textInputAction,
        textAlign: widget.textAlign,
        focusNode: widget.focusNode,
        hint: widget.hint,
        useClearIcon: widget.useClearIcon,
        controller: widget.controller,
        usePrefixSearch: widget.usePrefixSearch
    );
  }
}

class Input extends StatefulWidget {
  const Input({
    Key key,
    this.valueText = '',
    this.mask,
    this.validator,
    this.label,
    this.inputType,
    this.onFieldSubmitted,
    this.maxLenght,
    this.formKey,
    this.focusNode,
    this.textInputAction,
    this.hint,
    this.textAlign = TextAlign.center,
    this.useClearIcon,
    this.controller,
    this.usePrefixSearch,
  }) : super(key: key);

  final String valueText;
  final MaskTextInputFormatter mask;
  final String Function(String) validator;
  final String label;
  final TextInputType inputType;
  final void Function(String) onFieldSubmitted;
  final int maxLenght;
  final GlobalKey<FormState> formKey;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final TextAlign textAlign;
  final String hint;
  final bool useClearIcon;
  final TextEditingController controller;
  final bool usePrefixSearch;

  @override
  _InputState createState() => _InputState();
}

class _InputState extends State<Input> {
  TextEditingController  _controller;

  @override
  void initState() {
    _controller =  widget.controller != null ? widget.controller : TextEditingController();
    _controller.text = widget.valueText ?? '';
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Form(
        key: widget.formKey,
        child: TextFormField(
          controller: _controller,
          onFieldSubmitted: (String val) => {
            if (widget.formKey.currentState.validate()) {
              widget.onFieldSubmitted(val)
            }
          },
          inputFormatters: widget.mask != null ? [widget.mask] : [],
          validator: widget.validator,
          cursorColor: Colors.redAccent,
          keyboardType: widget.inputType,
          textAlign: widget.textAlign,
          focusNode: widget.focusNode,
          maxLength: widget.maxLenght,
          textInputAction: widget.textInputAction,
          decoration: InputDecoration(
            hintText: widget.hint,
            counterText: "",
            filled: true,
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[300])
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[100])
            ),
            fillColor: Colors.grey[200],
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey[400])
            ),
            suffixIcon: widget.useClearIcon ? IconButton(
              onPressed: () {
                _controller.clear();
              },
              icon: SvgPicture.asset('assets/icons/X.svg'),
              iconSize: 10,
            ) : null,
            prefixIcon: widget.usePrefixSearch ?
              Container(
                child: Icon(Icons.search, color: Colors.grey,)) : null,
            contentPadding:
                EdgeInsets.only(bottom: 9.0, left: 12.0, right: 12.0),
          ),
        ),
      ),
    );
  }
}

class InputAutocompleteBase extends StatefulWidget {
    const InputAutocompleteBase({
    Key key,
    this.mask,
    this.validator,
    this.label,
    this.inputType,
    this.onFieldSubmitted,
    this.maxLenght,
    this.formKey,
    this.focusNode,
    this.textInputAction,
    this.hint,
    this.textAlign = TextAlign.center,
    this.useClearIcon,
    this.controller,
  }) : super(key: key);

  final MaskTextInputFormatter mask;
  final String Function(String) validator;
  final String label;
  final TextInputType inputType;
  final void Function(String) onFieldSubmitted;
  final int maxLenght;
  final GlobalKey<FormState> formKey;
  final FocusNode focusNode;
  final TextInputAction textInputAction;
  final TextAlign textAlign;
  final String hint;
  final bool useClearIcon;
  final TextEditingController controller;

  @override
  _InputAutocompleteBaseState createState() => new _InputAutocompleteBaseState();
}

class _InputAutocompleteBaseState extends State<InputAutocompleteBase> {

  AutoCompleteTextField<String> searchTextField;
  TextEditingController  _controller;
  GlobalKey<AutoCompleteTextFieldState<String>> key;

  @override
  void initState() {
    _controller =  widget.controller != null ? widget.controller : TextEditingController();
    key = new GlobalKey();
    // AutoCompleteTextField(itemSubmitted: null, key: null, suggestions: null, itemBuilder: null, itemSorter: null, itemFilter: null)
    searchTextField = AutoCompleteTextField<String>(
      itemSubmitted: (item) {
        setState(() {
          searchTextField.textField.controller.text = item;
          _controller.text = item;
          widget.onFieldSubmitted(item);
        });
      },
      suggestions: allStation,
      itemBuilder: (context, item) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 3, vertical: 1.5),
          child: Text(
            item,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: Colors.grey[700],
              fontSize: 16.0
            )
          ),
        );
      },
    itemFilter: (item, query) {
      return item
      .toLowerCase()
      .startsWith(query.toLowerCase());
    },
    itemSorter: (a, b) {
      return a.compareTo(b);
    },
    key: key,
    style: new TextStyle(
      color: Colors.black,
      fontSize: 16.0
    ),
    decoration: InputDecoration(
      hintText: widget.hint,
      counterText: "",
      filled: true,
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey[300])
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey[100])
      ),
      fillColor: Colors.grey[200],
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.grey[400])
      ),
      suffixIcon: widget.useClearIcon ? IconButton(
        onPressed: () {
           _controller.clear();
           searchTextField.textField.controller.clear();
        },
        icon: SvgPicture.asset('assets/icons/X.svg'),
        iconSize: 10,
      ) : null,
      contentPadding:
          EdgeInsets.only(bottom: 9.0, left: 12.0, right: 12.0),
    ),
  );
    super.initState();
  }

  SimpleAutoCompleteTextField textField;
  bool showWhichErrorText = false;

  @override
  Widget build(BuildContext context) {
    return searchTextField;
  }
}
