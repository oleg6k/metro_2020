import 'package:flutter/material.dart';

/// Имя файла БД SQLite
const SQLITE_DB_NAME = 'metro_2020.db';

class System {
  /// Флаг, показывающий, что приложение запущено впервые
  static const FIRST_START = 'FIRST_START';

  /// Тёмная тема on/off
  static const DARK_THEME = 'DARK_THEME';

  /// Выбранный язык {en , ru}
  static const LOCALE = 'LOCALE';

  /// Выбранный язык {en , ru}
  static const PHONE = 'PHONE';
}

class PreferenceItem {
  bool selected;
  final String name;
  final String iconPath;

  selectedVal(bool val) {
    this.selected = val;
  }

  PreferenceItem({this.selected, this.name, this.iconPath});
}

List<PreferenceItem> preferencePath = [
  PreferenceItem(
    selected: false,
    name: 'Домой',
    iconPath: 'assets/preference/home.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'На работу',
    iconPath: 'assets/preference/portfolio-bagagge-tool.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Избранное 1',
    iconPath: 'assets/preference/star.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Избранное 2',
    iconPath: 'assets/preference/star.svg'
  ),
];

List<PreferenceItem> claim = [
  PreferenceItem(
    selected: false,
    name: 'ЧП',
    iconPath: 'assets/preference/high_importance.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Загруженность',
    iconPath: 'assets/preference/people.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Другое',
    iconPath: 'assets/preference/Vector-20.svg'
  ),
];

List<PreferenceItem> preferenceItem = [
  PreferenceItem(
    selected: true,
    name: 'Учет событий',
    iconPath: 'assets/preference/high_importance.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Низкая загруженность',
    iconPath: 'assets/preference/people.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Новые вагоны',
    iconPath: 'assets/preference/NEW.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Наличие USB',
    iconPath: 'assets/preference/usb.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Низкая мобильность',
    iconPath: 'assets/preference/assistive_technology.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Перехв. парковка',
    iconPath: 'assets/preference/parking.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Короткие переходы',
    iconPath: 'assets/preference/travelator.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Каршеринг',
    iconPath: 'assets/preference/car.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Сниженная стоимость',
    iconPath: 'assets/preference/low_price.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Прогулка',
    iconPath: 'assets/preference/walking.svg'
  ),
  PreferenceItem(
    selected: false,
    name: 'Темная тема',
    iconPath: 'assets/preference/Moon_and_Stars.svg'
  ),
];

const MaterialColor white = const MaterialColor(
  0xFFFFFFFF,
  const <int, Color>{
    50: const Color(0xFFFFFFFF),
    100: const Color(0xFFFFFFFF),
    200: const Color(0xFFFFFFFF),
    300: const Color(0xFFFFFFFF),
    400: const Color(0xFFFFFFFF),
    500: const Color(0xFFFFFFFF),
    600: const Color(0xFFFFFFFF),
    700: const Color(0xFFFFFFFF),
    800: const Color(0xFFFFFFFF),
    900: const Color(0xFFFFFFFF),
  },
);