import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro_2020/common/constants.dart';
import 'package:metro_2020/common/widgets/button_base.dart';
import 'package:metro_2020/common/widgets/checkbox_base.dart';
import 'package:metro_2020/common/widgets/input_base.dart';
import 'package:metro_2020/common/widgets/rounded_elevation_button.dart';
import 'package:metro_2020/common/station.dart';

class FromTo extends StatefulWidget {
  const FromTo({Key key}) : super(key: key);

  @override
  _FromToState createState() => _FromToState();
}

class _FromToState extends State<FromTo> {
  TextEditingController from;
  TextEditingController to;
  FocusNode focusNodeFrom;
  FocusNode focusNodeTo;

  final ValueNotifier<bool> confirmNot = ValueNotifier<bool>(false);

  @override
  void initState() {
    from = TextEditingController();
    to = TextEditingController();
    focusNodeFrom = FocusNode();
    focusNodeTo = FocusNode();
    focusNodeFrom.addListener(() {
      if(focusNodeFrom.hasFocus) {
        setState(() {});
      };
    });

    focusNodeTo.addListener(() {
      if(focusNodeTo.hasFocus) {
        setState(() {});
      };
    });

    super.initState();
  }
  @override
  void dispose() {
    focusNodeFrom.dispose();
    focusNodeTo.dispose();
    from.dispose();
    to.dispose();
    super.dispose();
  }

  getWidhtFrom(BuildContext context) {

    if(!focusNodeFrom.hasFocus && focusNodeTo.hasFocus) {
      return 0.0;
    }

    else if(focusNodeFrom.hasFocus) {
      return MediaQuery.of(context).size.width - 40;
    }
    
    else if(!focusNodeFrom.hasFocus) {
      return (MediaQuery.of(context).size.width - 76) / 2;
    }
  }

  getWidhtTo(BuildContext context) {

    if(focusNodeFrom.hasFocus && !focusNodeTo.hasFocus) {
      return 0.0;
    }

    else if(focusNodeTo.hasFocus) {
      return MediaQuery.of(context).size.width - 40;
    }
    
    else if(!focusNodeTo.hasFocus) {
      return (MediaQuery.of(context).size.width - 76) / 2;
    }
  }

  @override
  Widget build(BuildContext context) {

    return Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, right: 10),
                child: Align(
                  alignment: Alignment.topRight,
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    width: focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 0 : 48,
                    height: focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 0 : 48,
                    child: RoundedElevationButtonBase(
                      pathToIcon: 'assets/icons/Vector.svg',
                      onPress: () => _showPicker(context),
                    ),
                  )
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20 + 60.0, right: 10),
                child: Align(
                  alignment: Alignment.topRight,
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    width: focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 0 : 48,
                    height: focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 0 : 48,
                    child: RoundedElevationButtonBase(
                      pathToIcon: 'assets/icons/Union.svg',
                      onPress: () => _showReport(context),
                    ),
                  )
                ),
              ),
              AnimatedPadding(
                duration: Duration(milliseconds: 200),
                padding: EdgeInsets.only(bottom: 134 + 12.0  + (focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 40 : 0), right: 10),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: RoundedElevationButtonBase(
                    pathToIcon: 'assets/icons/Vector-18.svg',
                    onPress: () => {},
                  )
                ),
              ),
              AnimatedPadding(
                duration: Duration(milliseconds: 200),
                padding: EdgeInsets.only(bottom: 134 + 12.0 + (focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 40 : 0), left: 10),
                child: Align(
                  alignment: Alignment.bottomLeft,
                  child: RoundedElevationButtonBase(
                    pathToIcon: 'assets/map.svg',
                    onPress: () => {},
                  )
                ),
              ),
              // AnimatedContainer(
              //       duration: Duration(milliseconds: 200),
              //       width: to.text != '' || from.text != '' ? 0 : 48,
              //       height: to.text != '' || from.text != ''  ? 0 : 48,
              //       child: Align(
              //         child: RoundedElevationButtonBase(
              //           pathToIcon: 'assets/icons/train.svg',
              //           onPress: () {},
              //           color: Colors.redAccent,
              //           colorIcon: Colors.white,
              //         ),
              //       ),
              //     ),
              Align(
                alignment: Alignment.bottomCenter,
                child: AnimatedContainer(
                    duration: Duration(milliseconds: 200),
                    width: MediaQuery.of(context).size.width,
                    height: 134.0 + (focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 40 : 0),
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8),
                        topRight: Radius.circular(8),
                      ),
                    ),
                    child: Column(
                      children: [
                        SizedBox(height: 24),
                        // Авторесайц инпуты
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            AnimatedContainer(
                              duration: Duration(milliseconds: 200),
                              height: 44,
                              width: getWidhtFrom(context),
                              child: InputBase(
                                focusNode: focusNodeFrom,
                                textAlign: TextAlign.start,
                                hint: 'Откуда',
                                useClearIcon: true,
                                controller: from,
                                usePrefixSearch: focusNodeFrom.hasFocus,
                              )
                            ),
                            AnimatedContainer(
                              width: focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 0 : 36,
                              duration: Duration(milliseconds: 200),
                              child: SvgPicture.asset('assets/arrows.svg'),
                            ),
                            AnimatedContainer(
                              duration: Duration(milliseconds: 200),
                              height: 44,
                              width: getWidhtTo(context),
                              child: InputBase(
                                focusNode: focusNodeTo,
                                textAlign: TextAlign.start,
                                hint: 'Куда',
                                useClearIcon: true,
                                controller: to,
                                usePrefixSearch: focusNodeTo.hasFocus,
                              )
                            ),
                          ],
                          ///
                        ),
                        SizedBox(height: 24),
                        AnimatedContainer(
                          duration: Duration(milliseconds: 200),
                          height:  focusNodeFrom.hasFocus || focusNodeTo.hasFocus ? 0 : 30,
                          child: ValueListenableBuilder(
                            valueListenable: confirmNot,
                            builder: (_, val, __) => Row(
                              children: [
                                CheckBoxBase(
                                  bColor: Colors.blue,
                                  bgColor: confirmNot.value ? Colors.blue : Colors.white,
                                  cColor: Colors.white,
                                  value: !preferenceItem[0].selected,
                                  onChanged: () {
                                    setState(() {
                                      preferenceItem[0].selected = !preferenceItem[0].selected;
                                      confirmNot.value = !confirmNot.value;
                                    });
                                  }
                                ),
                                SizedBox(width: 16),
                                Text(
                                  'Учитывать мои предпочтения'
                                )
                              ],
                            ),
                          ),
                        ),
                        AnimatedContainer(
                          duration: Duration(milliseconds: 200),
                          height: focusNodeFrom.hasFocus? 90 : 0,
                          child: AutoCompleteListFromInput(controller: from)
                        ),
                        AnimatedContainer(
                          duration: Duration(milliseconds: 200),
                          height: focusNodeTo.hasFocus ? 90 : 0,
                          child: AutoCompleteListFromInput(controller: to)
                        )
                      ],
                    )
                  ),
                ),
              ]
            
    );
  }

  _showReport(BuildContext context) {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      transitionDuration: Duration(milliseconds: 450),
      barrierLabel: MaterialLocalizations.of(context).dialogLabel,
      barrierColor: Colors.black.withOpacity(0.5),
      pageBuilder: (context, _, __) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(24),
                  bottomRight: Radius.circular(24),
                ),
                child: Container(
                  padding: EdgeInsets.only(top: 40, left: 20, right: 20),
                  width: MediaQuery.of(context).size.width,
                  height: 90 + 56.0 + 78 + 24,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 16),
                      Preference(data: claim, centred: true),
                      SizedBox(height: 8),
                      ButtonBase(
                        text: "Отправить",
                        onPressed: () {
                          Navigator.of(context).pop();
                          showDialog(
                            context: context,
                            builder: (_) =>  AlertDialog(
                              backgroundColor: Theme.of(context).cardColor,
                              contentPadding: EdgeInsets.all(2),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.all(Radius.circular(10.0))
                              ),
                              title: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    'Отправленно',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16
                                    ),
                                  ),
                                  SizedBox(height: 16),
                                  Text(
                                    'Сообщение отправлено, благодарим за информацию',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(
                                      color: Colors.grey[700],
                                      fontWeight: FontWeight.w400,
                                      fontSize: 14
                                    ),
                                  ),
                                ]
                              ),
                            ),
                          );
                        },
                        borderColor: Colors.redAccent,
                        fillColor: Colors.redAccent,
                        textColor: Colors.white,
                      )
                    ]
                  ),
                )
              ),
            ]
        );
      },
      transitionBuilder: (context, animation, secondaryAnimation, child) {
        return SlideTransition(
          position: CurvedAnimation(
            parent: animation,
            curve: Curves.easeOut,
          ).drive(Tween<Offset>(
            begin: Offset(0, -1.0),
            end: Offset.zero,
          )),
          child: child,
        );
      },
    );
  }

  _showPicker(BuildContext context) {
    showGeneralDialog(
      context: context,
      barrierDismissible: true,
      transitionDuration: Duration(milliseconds: 450),
      barrierLabel: MaterialLocalizations.of(context).dialogLabel,
      barrierColor: Colors.black.withOpacity(0.5),
      pageBuilder: (context, _, __) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(24),
                  bottomRight: Radius.circular(24),
                ),
                child: Container(
                  padding: EdgeInsets.only(top: 40, left: 20, right: 20),
                  width: MediaQuery.of(context).size.width,
                  height: 400 + 56.0 + 78 + 24,
                  color: Colors.white,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Предпочтения',
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontSize: 16,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                      SizedBox(height: 16),
                      Preference(data: preferenceItem),
                      SizedBox(height: 24),
                      Text(
                        'Проложить маршрут',
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.subtitle1.copyWith(
                          fontSize: 16,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                      SizedBox(height: 16),
                      Preference(data: preferencePath),
                      SizedBox(height: 8),
                    ]
                  ),
                )
              ),
            ]
        );
      },
      transitionBuilder: (context, animation, secondaryAnimation, child) {
        return SlideTransition(
          position: CurvedAnimation(
            parent: animation,
            curve: Curves.easeOut,
          ).drive(Tween<Offset>(
            begin: Offset(0, -1.0),
            end: Offset.zero,
          )),
          child: child,
        );
      },
    );
  }
}


class Preference extends StatefulWidget {
  const Preference({
    Key key,
    @required this.data,
    this.centred = false
  }) : super(key: key);

  final List<PreferenceItem> data;
  final bool centred;

  @override
  _PreferenceState createState() => _PreferenceState();
}

class _PreferenceState extends State<Preference> {
  final maxItemInRow = 4;

  double calculatedPadding(BuildContext context) {
    final fullPadding = 170 + 14;
    final widthItem = 48;
    final paddingRight = (MediaQuery.of(context).size.width - (fullPadding + widthItem * maxItemInRow)) / maxItemInRow;
    return (MediaQuery.of(context).size.width - ((fullPadding - paddingRight) + widthItem * maxItemInRow)) / maxItemInRow;
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> rows = [];

    final paddingRight = calculatedPadding(context);

    final useDarkColor = widget.data.length == 4;

    for (var i = 0; i < 4; i++) {
      final List<Widget> row = [];

      widget.data.skip(i * maxItemInRow).take(maxItemInRow).forEach((e) {

        final usePadding = (widget.data.indexOf(e) + 1 ) % 4 != 0;

        row.add(
          Container(
            margin: EdgeInsets.only(right: usePadding ? paddingRight : 0.0, bottom: 16),
            child: Column(
              children:[
                ClipOval(
                  child: Container(
                    width: 48,
                    height: 48,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(24),
                    ),
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 250),
                      color: useDarkColor ? Colors.grey : e.selected ? Colors.red : Colors.grey[300],
                      child: Material(
                        color: Colors.transparent,
                        child: InkWell(
                          onTap: () => setState(() => e.selectedVal(!e.selected)),
                          child: Container(
                            child: Padding(
                              padding: const EdgeInsets.all(12),
                              child: SvgPicture.asset(
                                e.iconPath,
                                color: useDarkColor ? Colors.white : e.selected ? Colors.white : Colors.grey,
                              ),
                            )
                          ),
                        ),
                      ),
                    )
                  ),
                ),
                SizedBox(height: 4),
                Container(
                  width: 84,
                  child: Text(
                    e.name,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.subtitle1.copyWith(
                      fontSize: 14,
                      fontWeight: FontWeight.normal
                    ),
                  ),
                )
              ]
            ),
          )
        );
      });

      rows.add(
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: widget.centred ? MainAxisAlignment.center : MainAxisAlignment.start,
          children: row
        )
      );
    }

    return Column(
      children: rows,
    );
  }
}

class AutoCompleteListFromInput extends StatefulWidget {
  const AutoCompleteListFromInput({
    Key key,
    @required this.controller,
  }) : super(key: key);

  final TextEditingController controller;

  @override
  _AutoCompleteListFromInputState createState() => _AutoCompleteListFromInputState(controller);
}

class _AutoCompleteListFromInputState extends State<AutoCompleteListFromInput> {
  final TextEditingController controller;

  _AutoCompleteListFromInputState(this.controller);

  List<String> stations;

  @override
  void initState() {
    allStation.sort((a, b) => a.compareTo(b));
    controller.addListener(_update);
    super.initState();
  }

  _update() {
    setState(() {});
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: allStation.map((station) {
          if(station.toLowerCase().startsWith(controller.text.toLowerCase())) {
              return Material(
                color: Colors.transparent,
                child: InkWell(
                  onTap: () {
                    controller.text = station;
                    FocusScope.of(context).unfocus();
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: 12),
                    height: 67,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 24,
                          height: 15,
                          margin: EdgeInsets.only(top: 3),
                          child: SvgPicture.asset('assets/icons/M.svg')
                        ),
                        SizedBox(width: 8),
                        Container(
                          width: MediaQuery.of(context).size.width - 72,
                          decoration: BoxDecoration(
                            border: Border(
                              bottom: BorderSide(
                                width: 1,
                                color: Colors.grey
                              )
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                station,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500
                                ),
                              ),
                              SizedBox(height: 6),
                              Row(
                                children: [
                                  Container(
                                    width: 16,
                                    height: 16,
                                    decoration: BoxDecoration(
                                      color: Colors.yellow,
                                      borderRadius: BorderRadius.circular(3)
                                    ),
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    station,
                                    style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.normal
                                    ),
                                  ),
                                ],
                              ),
                            ]
                          ),
                        ),
                      ]
                    ),
                  )
                ),
              );
            } else {
              return Container();
            }
          }
        ).toList(),
      ),
    );
  }
}