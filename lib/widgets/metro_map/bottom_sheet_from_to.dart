import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro_2020/common/widgets/checkbox_base.dart';
import 'package:metro_2020/common/widgets/input_base.dart';

class BottomSheetFromTo extends StatefulWidget {
  BottomSheetFromTo({Key key}) : super(key: key);

  @override
  _BottomSheetFromToState createState() => _BottomSheetFromToState();
}

class _BottomSheetFromToState extends State<BottomSheetFromTo> {

  TextEditingController from;
  TextEditingController to;

  @override
  void initState() {
    from = TextEditingController();
    to = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(from.text);
    print(to.text);
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () => showModalBottomSheet(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8),
                topRight: Radius.circular(8),
              ),
            ),
            isScrollControlled: true, // Important: Makes content maxHeight = full device height
            context: context,
            builder: (context) {
            // Does not work
            return AnimatedPadding(
              padding: MediaQuery.of(context).viewInsets,
              duration: const Duration(milliseconds: 100),
              curve: Curves.decelerate,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 134,
                padding: EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8),
                    topRight: Radius.circular(8),
                  ),
                ),
                child: Column(
                  children: [
                    SizedBox(height: 24),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 44,
                          width: (MediaQuery.of(context).size.width - 76) / 2 ,
                          child: InputBase(
                            textAlign: TextAlign.start,
                            hint: 'Откуда',
                            useClearIcon: true,
                            controller: from,
                          )
                        ),
                        SvgPicture.asset('assets/arrows.svg'),
                        Container(
                          height: 44,
                          width: (MediaQuery.of(context).size.width - 76) / 2,
                          child: InputBase(
                            textAlign: TextAlign.start,
                            hint: 'Куда',
                            useClearIcon: true,
                            controller: to,
                          )
                        ),
                      ],
                    ),
                    SizedBox(height: 24),
                    Row(
                      children: [
                        CheckBoxBase(
                          bColor: Colors.blue,
                          bgColor: Colors.blue,
                          cColor: Colors.white,
                          value: true, onChanged: () => {}
                        ),
                        SizedBox(width: 16),
                        Text(
                          'Учитывать мои предпочтения'
                        )
                      ],
                    )
                  ],
                )
              )
            );
          }
        ),
        child: Container(
          width: 48,
          height: 48,
          padding: const EdgeInsets.all(14),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(24),
            color: Colors.white,
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.black54,
                  blurRadius: 7,
                  offset: Offset(0, 2)
              )
            ],
          ),
          child: SvgPicture.asset('assets/icons/Vector-1.svg')
        ),
      ),
    );
  }
}
