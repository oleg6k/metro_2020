import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metro_2020/common/functions.dart';
import 'package:metro_2020/common/widgets/scaffold_base.dart';
import 'package:metro_2020/widgets/FromTo.dart';
import 'package:zoom_widget/zoom_widget.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Metro extends StatefulWidget {
  const Metro({Key key}) : super(key: key);

  @override
  _MetroState createState() => _MetroState();
}

class _MetroState extends State<Metro> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();
  final State state = null;
  String start;
  String finish;
  SvgPicture svgImg = SvgPicture.asset('assets/mtHack2020.svg');
  String svgImgString;
  String svgImgStringDefault;

  @override
  Widget build(BuildContext context) {
    print(start);
    print(finish);
    return ScaffoldBase(
      body: Stack(
        children: [
          Zoom(
            width: 2250,
            height: 3029,
            child: GestureDetector(
              child: svgImg,
              onTapDown: (details) async {
                FocusScope.of(context).unfocus();
                String svgImgStringDefaultLoaded = svgImgStringDefault == null ? await DefaultAssetBundle.of(context).loadString("assets/mtHack2020.svg") : svgImgStringDefault;
                String stationID = await getStationID(details.localPosition.dx, details.localPosition.dy, context);
                setState(() {
                  svgImgStringDefault = svgImgStringDefaultLoaded;
                  svgImgString = svgImgString == null ? svgImgStringDefault : svgImgString;

                  if (start != null && finish != null) {
                    start = null;
                    finish = null;
                    svgImg = SvgPicture.string(svgImgStringDefault);
                    svgImgString = svgImgStringDefaultLoaded;
                  } else if (start == null) {
                    start = stationID;
                    svgImg = SvgPicture.string(setActiveStation(stationID, svgImgString));
                  } else {
                    finish = stationID;
                    svgImg = SvgPicture.string(setActiveStation(stationID, svgImgString));
                  }
                });

                if (start != null && finish != null) {
                  await pathFinder(start, finish, context);
                }
              },
            ),
          )
          ,FromTo()
        ],
      )
    );
  }

  String setActiveStation(id, svgString) {
    RegExp stationRegExp = RegExp('(?=<).*station-$id.*(?=>)', caseSensitive: false, multiLine: true);
    String stationString = stationRegExp.stringMatch(svgString).toString();
    RegExp selfRegExp = RegExp(stationString, caseSensitive: false, multiLine: true);
    RegExp fillStringRegExp = RegExp(r'(?<=fill:#)(\w)*');
    String newStationString = stationString.replaceFirst(fillStringRegExp, "11f902");
    return svgString.replaceAll(selfRegExp, newStationString);
  }

  Future<void> pathFinder(start, finish, context) async {
    String svgString = svgImgString;
    //TODO:
    List<String> ids = [start, finish];
    for (String id in ids) {
      svgString = setActiveStation(id, svgString);
    }
    setState(() {
      svgImg = SvgPicture.string(svgString);
      svgImgString = svgString;
    });

//    String modelString = await DefaultAssetBundle.of(context).loadString("assets/model_solver_model.json");
//    final Map<String, dynamic> model = json.decode(modelString);
  }
}

// var svg = SvgPicture.asset('assets/mtHack2020.svg',color: Colors.greenAccent);

Future<String> getStationID(x, y, context) async {
  String res;
  try {
    String data = await DefaultAssetBundle.of(context).loadString("assets/labels.json");
    final Map<String, dynamic> jsonResult = json.decode(data);
    res = jsonResult.keys.firstWhere((key) {
      return jsonResult[key]['x'] < x && jsonResult[key]['xMax'] > x && jsonResult[key]['y'] > y && jsonResult[key]['yMin'] < y;
    });
  } catch (e) {}

  return res;
}

changeElementColors() {}

//
//Future<int> travellingSalesmanProblem(graph, int startIndex, int finishIndex)async{
//
//  int minPath = 2147483647;
//
//  final bagOfItems = new List<int>.generate(graph.length, (i) => i + 1), combos = Combinations(3, bagOfItems);
//  for (final combo in combos()) {
//
//   int currentPathWeight = 0;
//   int k = startIndex;
//
//   for(final j in combo){
//     print(j);
//     if(j>graph.length-1){
//       break;
//     }
//     currentPathWeight += graph[k][j];
//     if (k == finishIndex){
//       break;
//     }
//   }
//
//   minPath = [minPath, currentPathWeight].reduce(min);
//
//  }
//
//  return minPath;
//
//
//}

//
//final Paint black = Paint()
//  ..color = Colors.black
//  ..strokeWidth = 1.0
//  ..style = PaintingStyle.stroke;
//
//class PathTestPainter extends CustomPainter {
//  PathTestPainter(String path) : p = parseSvgPathData(path);
//
//  final Path p;
//
//  @override
//  bool shouldRepaint(PathTestPainter oldDelegate) => true;
//
//  @override
//  void paint(Canvas canvas, Size size) {
//    canvas.drawPath(p, black);
//  }
//}
