import 'package:flutter/material.dart';
import 'package:metro_2020/common/metro_svg_string.dart';
import 'package:path_drawing/path_drawing.dart';
import 'package:touchable/touchable.dart';

MetroElementDraw executor(String inputString) {
  final arrayOptions = inputString;
  String d;
  String id;
  if ((arrayOptions.startsWith('g') || arrayOptions.startsWith('path')) && (arrayOptions.indexOf('rect') == -1)) {
    final dIndex = arrayOptions.indexOf('d=');
    final idIndex = arrayOptions.indexOf('id=');
    final styleIndex = arrayOptions.indexOf('style=');
    d = arrayOptions.substring(dIndex, styleIndex == -1 ? arrayOptions.length : styleIndex);
    id = arrayOptions.substring(idIndex, arrayOptions.indexOf(' ', idIndex));}

  return MetroElementDraw(path: d, id: id);
}

stationCause(String string){
  return string.startsWith('path') && (string.indexOf('station-') == -1) && (string.indexOf('d=') == -1) && (string.indexOf('id=') == -1);
}
stationToStationLineCause(String string){
  return string.startsWith('g') && (string.indexOf('station-') == -1) && (string.indexOf('d=') == -1) && (string.indexOf('id=') == -1);
}

List<MetroElementDraw> svgToPaths() {
  final arrPath = metroRaw.split('<');
  final List<MetroElementDraw> pathArr = [];
  arrPath.forEach((element) {
    try {
      pathArr.add(executor(element));
    } catch (e) {
      // print('$e, $element');
    }
  });

  pathArr.removeWhere((element) => element.id == null && element.path == null);

  return pathArr;
}

class MetroElementDraw {
  String id;
  String path;

  MetroElementDraw({this.id, this.path});

  @override
  String toString() {
    return 'path: $path, id: $id';
  }
}

final Paint black = Paint()
  ..color = Colors.black
  ..strokeWidth = 1.0
  ..style = PaintingStyle.stroke;

class PathPainter extends CustomPainter {
  PathPainter() : p = parseSvgPathData('');

  final Path p;

  @override
  bool shouldRepaint(PathPainter oldDelegate) => true;

  @override
  void paint(Canvas canvas, Size size) {
    print('INSIDE paint');
    final List<MetroElementDraw> metroList = svgToPaths();
    print(metroList);

    Paint paint = Paint()
      ..style = PaintingStyle.fill
      ..strokeWidth = 8.0;

    // Scale each path to match canvas size
    var xScale = size.width / 250;
    var yScale = size.height / 400;
    final Matrix4 matrix4 = Matrix4.identity();

    matrix4.scale(xScale, yScale);

    metroList.forEach((metro) {
      Path path = parseSvgPathData(metro.path);
      path.transform(matrix4.storage);

      canvas.drawPath(
        path.transform(matrix4.storage),
        paint
      );
    });
  }
}
