import 'package:flutter/cupertino.dart';

class Rectangle extends CustomPainter {
  @required final double dx;
  @required final double dy;
  @required final double width;
  @required final double height;
  @required final Color fillColor;

  Rectangle({this.dx, this.dy, this.width, this.height, this.fillColor});


  @override
  void paint(Canvas canvas, Size size) {
    var paint1 = Paint()
      ..color = fillColor == null ? Color(0xff995588) : fillColor
      ..style = PaintingStyle.fill;
    canvas.drawRect(Offset(dx, dy) & Size(width, height), paint1);
  }
 
  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
