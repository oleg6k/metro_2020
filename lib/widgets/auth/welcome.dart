import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:metro_2020/common/widgets/button_base.dart';
import 'package:metro_2020/common/widgets/scaffold_base.dart';
import 'package:metro_2020/widgets/auth/phone_Input.dart';
import 'package:metro_2020/widgets/metro_map/metro.dart';

class Welcome extends StatelessWidget {
  const Welcome({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScaffoldBase(
      useDrawer: false,
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height - (345),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/metro.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 345,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                )
              ),
              width: MediaQuery.of(context).size.width,
              child: Padding( padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: Text(
                        'Предпочтения',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 24
                        ),
                      ),
                    ),
                    Text(
                      'Для предоставления рекомендаций на основе личных предпочтений необходима авторизация.',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        color: Colors.grey[400],
                        height: 1
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          ButtonBase(
                            fillColor: Colors.redAccent,
                            textColor: Colors.white,
                            text: 'Вход',
                            onPressed: ()  {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) => Metro())
                              );
                            }
                          ),
                          ButtonBase(
                            fillColor: Colors.white,
                            textColor: Colors.blue,
                            borderColor: Colors.blue,
                            splashColor: Colors.grey,
                            text: 'В другой раз',
                            onPressed: () => {},
                          ),
                        ],
                      ),
                    ),
                    Text(
                      'Если у вас уже есть аккаунт, авторизуйтесь удобным способом:',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        color: Colors.grey[400],
                        height: 1
                      ),
                    ),
                    SizedBox(height: 14),
                    Container(
                      height: 48,
                      width: MediaQuery.of(context).size.width,
                      child: SvgPicture.asset(
                        'assets/social.svg',
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(height: 26)
                  ]
                )
              ),
            ),
          ),
        ]
      ),
    );
  }
}