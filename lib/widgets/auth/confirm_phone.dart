import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:injector/injector.dart';
import 'package:metro_2020/common/widgets/input_base.dart';
import 'package:metro_2020/common/widgets/scaffold_base.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:metro_2020/states/system_bloc.dart';
import 'package:metro_2020/widgets/metro_map/metro.dart';

var maskPhoneFormatter = new MaskTextInputFormatter(mask: '+7 (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });

class ConfirmPhone extends StatefulWidget {
  const ConfirmPhone({Key key}) : super(key: key);

  @override
  _CConfirmPhoneState createState() => _CConfirmPhoneState();
}

class _CConfirmPhoneState extends State<ConfirmPhone> {
  final ValueNotifier<bool> confirmNot = ValueNotifier<bool>(false);

  SystemBloc systemBloc;
  FocusNode fnOne;
  FocusNode fnTwo;
  FocusNode fnThree;
  FocusNode fnFour;
  

  @override
  void initState() {
    systemBloc = Injector.appInstance.getDependency<SystemBloc>();
    fnOne = FocusNode();
    fnTwo = FocusNode();
    fnThree = FocusNode();
    fnFour = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    fnOne.dispose();
    fnTwo.dispose();
    fnThree.dispose();
    fnFour.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return ScaffoldBase(
      useDrawer: false,
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height - (237),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/metro.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 237 + 22.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                )
              ),
              width: MediaQuery.of(context).size.width,
              child: Padding( padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: Text(
                        'Введите код из SMS',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 24
                        ),
                      ),
                    ),
                    SizedBox(height: 12),
                    StreamBuilder(
                      stream: systemBloc.data,
                      builder: (BuildContext context, AsyncSnapshot<SystemData> snapshot) {
                        return Text(
                          'На ваш номер ${snapshot?.data?.phone} отправлен код подтверждения',
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Colors.grey[400],
                            height: 1
                          ),
                        );
                      }
                    ),
                    SizedBox(height: 8),
                    Text(
                      'Изменить номер телефона',
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 13,
                        color: Colors.blue,
                        height: 1
                      ),
                    ),
                    SizedBox(height: 12),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.only(right: 16),
                          width: 44,
                          height: 44,
                          child: InputBase(
                            maxLenght: 1,
                            valueText: '',
                            focusNode: fnOne,
                            inputType: TextInputType.number,
                            onFieldSubmitted: (String val) {
                              if(val != null || val != '') {
                                print('tutut');
                                fnOne.unfocus();
                                FocusScope.of(context).requestFocus(fnTwo);
                              }
                            },
                          ),
                        ),
                       Container(
                          width: 44,
                          height: 44,
                          margin: EdgeInsets.only(right: 16),
                          child: InputBase(
                            focusNode: fnTwo,
                            maxLenght: 1,
                            valueText: '',
                            inputType: TextInputType.number,
                            onFieldSubmitted: (String val) {
                              if(val != null || val != '') {
                                fnTwo.unfocus();
                                FocusScope.of(context).requestFocus(fnThree);
                              }
                            },
                          ),
                        ),
                        Container(
                          width: 44,
                          height: 44,
                          margin: EdgeInsets.only(right: 16),
                          child: InputBase(
                            focusNode: fnThree,
                            maxLenght: 1,
                            valueText: '',
                            inputType: TextInputType.number,
                            onFieldSubmitted: (String val) {
                              if(val != null || val != '') {
                                fnThree.unfocus();
                                FocusScope.of(context).requestFocus(fnFour);
                              }
                            },
                          ),
                        ),
                        Container(
                          width: 44,
                          height: 44,
                          child: InputBase(
                            focusNode: fnFour,
                            maxLenght: 1,
                            valueText: '',
                            inputType: TextInputType.number,
                            onFieldSubmitted: (String val) {
                              if(val != null || val != '') {
                                fnFour.unfocus();
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => Metro())
                                );
                              }
                            },
                          ),
                        ),
                      ]
                    ),
                    SizedBox(height: 12),
                    Text(
                      'Запросить код подвторно',
                      style: TextStyle(
                        fontWeight: FontWeight.normal,
                        fontSize: 13,
                        color: Colors.blue,
                        height: 1
                      ),
                    ),
                  ]
                )
              ),
            ),
          ),
        ]
      ),
    );
  }
}
