import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:injector/injector.dart';
import 'package:metro_2020/common/widgets/checkbox_base.dart';
import 'package:metro_2020/common/widgets/input_base.dart';
import 'package:metro_2020/common/widgets/scaffold_base.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:metro_2020/states/system_bloc.dart';
import 'package:metro_2020/widgets/auth/confirm_phone.dart';

var maskPhoneFormatter = new MaskTextInputFormatter(mask: '+7 (###) ###-##-##', filter: { "#": RegExp(r'[0-9]') });

class PhoneInput extends StatefulWidget {
  const PhoneInput({Key key}) : super(key: key);

  @override
  _PhoneInputState createState() => _PhoneInputState();
}

class _PhoneInputState extends State<PhoneInput> {
  final ValueNotifier<bool> confirmNot = ValueNotifier<bool>(false);
  SystemBloc systemBloc;

  @override
  void initState() {
    systemBloc = Injector.appInstance.getDependency<SystemBloc>();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return ScaffoldBase(
      useDrawer: false,
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height - (237),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/metro.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 237 + 22.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(8),
                  topRight: Radius.circular(8),
                )
              ),
              width: MediaQuery.of(context).size.width,
              child: Padding( padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 14),
                      child: Text(
                        'Введите номер телефона',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 24
                        ),
                      ),
                    ),
                    SizedBox(height: 12),
                    Text(
                      'Вам придет SMS с кодом подтверждения',
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 18,
                        color: Colors.grey[400],
                        height: 1
                      ),
                    ),
                    SizedBox(height: 22),
                    StreamBuilder(
                      stream: systemBloc.data,
                      builder: (BuildContext context, AsyncSnapshot<SystemData> snapshot) {
                        return InputBase(
                          mask: maskPhoneFormatter,
                          valueText: snapshot?.data?.phone != null ? snapshot.data.phone : '+7',
                          inputType: TextInputType.phone,
                          validator: (String val) {
                            if(val.length < 18) {
                              return 'Это не телефонный номер';
                            }
                            return null;
                          },
                          onFieldSubmitted: (String val) async {
                            await systemBloc.setPhone(val);
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => ConfirmPhone())
                            );
                          },
                        );
                      }
                    ),
                    SizedBox(height: 23),
                    ValueListenableBuilder(
                      valueListenable: confirmNot,
                      builder: (__, confirm, _) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CheckBoxBase(
                              value: confirm,
                              onChanged: () => confirmNot.value = !confirmNot.value
                            ),
                            SizedBox(width: 15),
                            RichText(
                              text: TextSpan(
                                children: <TextSpan>[
                                  TextSpan(
                                    text: 'Я ознакомлен и согласен с',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w400,
                                      fontSize: 13,
                                      color: Colors.grey[400],
                                      height: 1
                                    ),
                                  ),
                                  TextSpan(
                                    text: '\nполитикой обработки персональных данных',
                                    style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.w400,
                                      fontSize: 13,
                                      height: 1,
                                      color: Colors.blue
                                    )
                                  )
                                ]
                              ),
                            ),
                          ]
                        );
                      }
                    ),
                    SizedBox(height: 14),
                  ]
                )
              ),
            ),
          ),
        ]
      ),
    );
  }
}
