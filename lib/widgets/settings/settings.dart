import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:metro_2020/common/widgets/button_base.dart';
import 'package:metro_2020/common/widgets/input_base.dart';
import 'package:metro_2020/common/widgets/scaffold_base.dart';
import 'package:metro_2020/widgets/auth/welcome.dart';

class Settings extends StatelessWidget {
  const Settings({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Container(
            color: Colors.white,
            child: Row(
              children: [
                ClipOval(
                  child: Container(
                    width: 48,
                    height: 48,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(24),
                      color: Colors.grey[300],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.asset('assets/no_avatar.png'),
                    ),
                  ),
                ),
                SizedBox(width: 8),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Евгений',
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w700,
                        fontSize: 18
                      ),
                    ),
                    Text(
                      '+7 (920) 624 15 04',
                      style: TextStyle(
                        color: Colors.grey[500],
                        fontWeight: FontWeight.w400,
                        fontSize: 14
                      ),
                    )
                  ]
                )
              ],
            )
          ),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16),
                Text(
                  'Аккаунт',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 16
                  )
                ),
                SizedBox(height: 14),
                InputBase(
                  label: 'Имя',
                  textAlign: TextAlign.start,
                  valueText: 'Евгений',
                ),
                SizedBox(height: 26),
                InputBase(
                  label: 'Фамилия',
                  textAlign: TextAlign.start,
                  valueText: 'Сергеевич',
                ),
                SizedBox(height: 26),
                InputBase(
                  label: 'Отчество',
                  textAlign: TextAlign.start,
                  valueText: 'Денисов',
                ),
                SizedBox(height: 26),
                InputBase(
                  label: 'Дата рождения',
                  textAlign: TextAlign.start,
                  valueText: '15.05.2000',
                ),
                SizedBox(height: 26),
                InputBase(
                  label: 'Форма обращения',
                  textAlign: TextAlign.start,
                  valueText: 'Имя, Отчество',
                ),
                SizedBox(height: 26),
                ButtonBase(
                  text: 'Выйти из профиля',
                  fillColor: Colors.white,
                  textColor: Colors.redAccent,
                  splashColor: Colors.grey[300],
                  onPressed: () {
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => Welcome()),
                      (route) => false
                    );
                  },
                ),
                SizedBox(height: 26),
              ]
            ),
          ),
        ),
      ),
    );
  }
}